#include "mbed.h"
#include "DHT.h"
#include "Adafruit_ST7735.h"
//#include "rtos.h"
#include "ds1307.h"
// display
Adafruit_ST7735 tft(D11, D12, D13, D10, D4, D9); // MOSI, MISO, SCLK, SSEL, TFT_DC, TFT_RST



//DS1307 rtc;
//DS1307 rtc(PA_0,PA_1);
//debug 
// uart 2 CONTROLLA CAPRA!
Serial debugUSB(PA_2, PA_3); // tx, rx
// bluetooth
// UART 1 
Serial device(PA_9, PA_10);  // tx, rx
//Serial device(PB_6, PA_10);  // tx, rx
//DigitalOut LED1(LED1);

DigitalOut myled(PC_8);
DigitalOut myled2(PC_11);
DigitalOut myled3(PC_12);
DigitalOut myled4(PD_2);
// usa un led che io ho usato il beeper per che sono povero
DigitalOut GardenLed(PC_6);
// photodiodo collegato ad arduino che funziona come ADC
InterruptIn photodiode(PC_9);
InterruptIn blouthootIcon(PC_10);

// sensore temperatura ed umidita pin 4 a massa! 
DHT sensor(PA_8,SEN11301P); // Use the SEN11301P sensor
//DigitalOut FanPWM (PA_11);
PwmOut FanPWM (PA_11);
//AnalogIn input(PA_0);
float sample = 0.00f;
// 4 second period
const int buffer_size = 9;

char rx_buffer[buffer_size+1];
char tx_buffer[buffer_size+1] = "y0000000";;

volatile int tx=0;
volatile int rx=0;

bool bufferFull=false;

// Time
 int sec = 0;
 int min = 0;
 int hours = 0;
 int day = 1; //sunday
 int date = 1;
 int month = 1;
 int year = 12; //2012
 //

//enum

typedef struct {
    char *Name;
    float Speed;
} fanSpeedStruct;

fanSpeedStruct fanSpeed[5]= {{"off",0.00f},{"slow",0.25f},{"medium",0.50f},{"high",0.75f},{"Max",1.00f}};

void Rx_interrupt();
//void testdrawtext(char *text, uint16_t color);
// scrive del testo sul display 
void testdrawtext(char *text, uint16_t color )
{
    tft.fillScreen(ST7735_BLACK);
    tft.setTextSize(2);
    tft.setCursor(0, 0);
    tft.setTextColor(color);
    tft.setTextWrap(true);
    tft.printf("%s",text);
}



void PhotodiodShowIcon()
{
    tft.drawCircle(20, 6, 4, ST7735_WHITE);
}

void PhotodiodHideIcon()
{
    tft.drawCircle(20, 6, 4, ST7735_BLACK);
}

void led2on(void)
{
    GardenLed = 1;
    PhotodiodShowIcon();
    
}

void led2off(void)
{
    GardenLed = 0;
    PhotodiodHideIcon();
}



bool isRotating()
{
    float media=0.00f;
    for (int i = 0; i < 10; i++) {
        //media += input.read();
        printf(" media no sum : %f \r \n",media);
    }
    media = media / 10;
    printf(" media : %f \r \n",media);
    //float rpm = (1000000/media)*60;
    //printf(" rpm : %i \r \n",rpm);
    if (media == 1.00f) {
        return true;
    } else {
        return false;
    }
}



void BlouetoothIcon()
{
    //tft.setCursor(0, 2);
    tft.drawLine(3,3,11,11,ST7735_WHITE);
    tft.drawLine(11,3,3,11,ST7735_WHITE);
    tft.drawLine(7,1,7,13,ST7735_WHITE);
    tft.drawLine(7,1,11,3,ST7735_WHITE);
    tft.drawLine(7,13,11,11,ST7735_WHITE);
    //tft.drawLine(0,80,125,80,ST7735_WHITE);
}

//bool shwoIcon = false;
void BltHideIcon(void)
{
    tft.drawRect(0,0,12,14,ST7735_BLACK);
    tft.fillRect(0,0,12,14,ST7735_BLACK);
    //shwoIcon = false;
}

void BltShowIcon(void)
{
    //blouthootIcon.fall(NULL);
    //blouthootIcon.rise(NULL);
    //if (shwoIcon)
    BlouetoothIcon();
    //shwoIcon = true;
    //wait(500);
    //blouthootIcon.fall(&BltHideIcon);
    //blouthootIcon.rise(&BltShowIcon);
}

void test_RTC_hw(int test) {
     if (test == 0) debugUSB.printf("Last R/W operaion passed!\n\r");
     else debugUSB.printf("Last R/W operation failed!\n\r");
 }

//void some_work()
//{
//    while(true) {
//        debugUSB.printf("OOH IT IS A NEW FUNKIN THREAD!!!!!");
//        Thread::wait(1000);
//    }
//}

//void led2_thread()
//{
//    while (true) {
//        myled = !myled;
//        Thread::wait(1000);
//   }
//}

//void Rx_interrupt() {
//  
//    /*Loop if there is stuff to read and space in buffer */
//    while ((device.readable())) {
//        //store the char in the buffer
//        rx_buffer[0] = device.getc();
//        debugUSB.putc(rx_buffer[0]);
//    }
//   
//    
//}
 

void Rx_interrupt()
{
    
//    led1=1;
// Loop just in case more than one character is in UART's receive FIFO buffer
// Stop if buffer full
    //debugUSB.printf("device.readable : %d\n\r",device.readable());
    while ((device.readable()) && (((rx + 1) % buffer_size))) {
//      while ((device.readable()) && (((rx) % buffer_size))) {
//      while ((pc.readable()) && (((rx + 1) < buffer_size))) {
        rx_buffer[rx] = device.getc();
        //debugUSB.putc(rx_buffer[rx]);
//        device.printf("%d\n",(((rx + 1) % buffer_size)));
//        debugUSB.printf("pc.readable");
//        debugUSB.printf("%i", rx);
//        debugUSB.printf(" - ");
//        debugUSB.printf("%s", rx_buffer);
//        debugUSB.printf(" | ");
// Uncomment to Echo to USB serial to watch data flow
//        monitor_device.putc(rx_buffer[rx_in]);
        //rx = (rx + 1) % buffer_size;
        rx++;
        if ((rx + 1)  == buffer_size ) {
            debugUSB.printf("setto buffer full to true \n\r ");
            bufferFull=true;
        }
    }
//    led1=0;
    return;
}

int main()
{
    //setup();
    device.baud(9600);   
    tft.initR(INITR_BLACKTAB);   // initialize a ST7735S chip, black tab
    testdrawtext("LOADING...",ST7735_WHITE);
    wait_ms(500);
    //debugUSB.printf("Prima di attach rx \n\r");
    //debugUSB.printf("device.readable MAIN: %d\n\r",device.readable());
    device.attach(&Rx_interrupt, Serial::RxIrq);
    //device.attach(Rx_interrupt, Serial::RxIrq);
    //device.attach(&Tx_interrupt, Serial::TxIrq);
    debugUSB.printf("Hello Word! Maria Bucciero - 2018");
    photodiode.mode(PullDown);
    photodiode.rise(&led2on);
    photodiode.fall(&led2off);

    //blouthootIcon
    blouthootIcon.mode(PullDown);
    blouthootIcon.rise(&BltShowIcon);
    blouthootIcon.fall(&BltHideIcon);
    //FanPWM = 1.0f;
    // Setto fan speed. Il perioso è 500 ms/1000 ms
    FanPWM.period(0.5f);
//    FanPWM.write(0.00f);
//    FanPWM.write(fanSpeed[2].Speed);
    int err;
    float oldTemp,oldHumidity = 0.00f;
    float oldFanSpeed = 5.00f;
    // test RTC HW
    //test_RTC_hw(rtc.gettime( &sec, &min, &hours, &day, &date, &month, &year));
    // free thread
//    Thread free_function_thread;
//    free_function_thread.start(some_work);
    //osStatus errThread = free_function_thread.start(&some_work);
    //   if (errThread) {
    //      // treat error here if any
    //      debugUSB.printf("OOH my fucking thread has a problem :(");
    //   }
    //free_function_thread.join();
    //
    tft.fillScreen(ST7735_BLACK);
    //PhotodiodIcon();
    //BlouetoothIcon();
    //tft.setTextColor(ST7735_WHITE);
    //tft.drawLine(0,65,125,65,ST7735_WHITE);
    //
    tft.setTextSize(1);
    tft.setCursor(50, 2);
    tft.setTextColor(ST7735_WHITE);
    tft.setTextWrap(true);
    //tft.printf("10:38");
    //
    tft.setTextSize(2);
    tft.setCursor(0, 20);
    tft.setTextColor(ST7735_WHITE);
    tft.setTextWrap(true);
    tft.printf("Temp:");
    tft.setCursor(110, 20);
    tft.printf("C");
    //
    tft.drawLine(0,38,125,38,ST7735_WHITE);
    tft.setTextSize(2);
    tft.setCursor(0, 42);
    tft.setTextColor(ST7735_WHITE);
    tft.printf("Fan:");
    tft.drawLine(0,82,125,82,ST7735_WHITE);
    tft.setCursor(0, 64);
    tft.setTextColor(ST7735_WHITE);
    tft.printf("Hum.:");
    //
    tft.drawLine(10,120,115,120,ST7735_WHITE);
    tft.drawLine(65,90,65,150,ST7735_WHITE);
    tft.setTextSize(1);
    tft.setTextColor(ST7735_WHITE);
    // room 1
    tft.setCursor(30, 135);
    tft.printf("OFF");
    //
    tft.setCursor(30, 100);
    tft.printf("OFF");
        //
    tft.setCursor(80, 135);
    tft.printf("OFF");
        //
    tft.setCursor(80, 100);
    tft.printf("OFF");
    //tft.fillRect(0,60,125,1,ST7735_WHITE);
    while(1) {
        //device.printf("%d\n",bufferFull);
        //char str[80];

        err = sensor.readData();
        if (err == 0) {
            //printf("Temperature is %4.2f C \r\n",sensor.ReadTemperature(CELCIUS));
            float test = sensor.ReadTemperature(CELCIUS);
            if (test != oldTemp) {
                oldTemp = test;
                //tft.fillScreen(ST7735_BLACK);
                tft.setTextSize(3);
                tft.setCursor(32, 14);
                tft.setTextColor(ST7735_WHITE);
                tft.drawRect(65,14,38,23,ST7735_BLACK);
                tft.fillRect(65,14,38,23,ST7735_BLACK);
                tft.printf("%4.0f",oldTemp);
                //printf("tft.print : %f \r \n",oldTemp);

            }
            float Humidity = sensor.ReadHumidity();
            if (oldHumidity != Humidity) {
                oldHumidity = Humidity;
                //tft.fillScreen(ST7735_BLACK);
                tft.setTextSize(2);
                tft.setCursor(60, 64);
                tft.setTextColor(ST7735_WHITE);
                tft.drawRect(59,63,50,17,ST7735_RED);
                tft.fillRect(59,63,50,17,ST7735_RED);
                tft.printf("%3.0f%%",oldHumidity);
                //printf("tft.print : %f \r \n",oldTemp);
            }
            //printf("Temperature is %f C \r\n",test);
            //sprintf(str, "%f", 3.123);
            //strstrcat(str, " C ");
//            strcat(str, test);
            //testdrawtext(str,ST7735_GREEN);
            tft.setTextSize(2);
            tft.setCursor(50, 42);
            //printf("oldFanSpeed = %f  | fanSpeed = %f",oldFanSpeed,fanSpeed[0].Speed);
            if (test <= 24.00f && oldFanSpeed != fanSpeed[0].Speed) {
                FanPWM.write(fanSpeed[0].Speed);
                oldFanSpeed = fanSpeed[0].Speed;
                //printf(" IN oldFanSpeed = %f  | fanSpeed = %f",oldFanSpeed,fanSpeed[0].Speed);
                //tft.setCursor(40, 70);
                tft.drawRect(45,42,75,16,ST7735_BLACK);
                tft.fillRect(45,42,75,16,ST7735_BLACK);
                tft.setTextColor(ST7735_WHITE);
                tft.printf(fanSpeed[0].Name);
                //printf(fanSpeed[0].Name);
            }
            if ((test > 24.00f && test <= 25.00f) && oldFanSpeed != fanSpeed[2].Speed) {
                FanPWM.write(fanSpeed[2].Speed);
                oldFanSpeed = fanSpeed[2].Speed;
                //tft.setCursor(40, 70);
                tft.drawRect(45,42,75,16,ST7735_BLACK);
                tft.fillRect(45,42,75,16,ST7735_BLACK);
                tft.setTextColor(ST7735_GREEN);
                tft.printf(fanSpeed[2].Name);
                //printf(isRotating() ? "true" : "false");
                //printf("sample : %f \r \n",sample);
                //printf("\r\n");
            }
            if ((test > 25.00f && test <= 28.00f) && oldFanSpeed != fanSpeed[3].Speed ) {
                FanPWM.write(fanSpeed[3].Speed);
                oldFanSpeed = fanSpeed[3].Speed;
                //tft.setCursor(40, 70);
                tft.drawRect(45,42,75,16,ST7735_BLACK);
                tft.fillRect(45,42,75,16,ST7735_BLACK);
                tft.setTextColor(ST7735_YELLOW);
                //tft.drawRect(40,70,125,20,ST7735_BLACK);
                //tft.fillRect(40,70,125,20,ST7735_BLACK);
                tft.printf(fanSpeed[3].Name);
                //sample = input.read();
                //printf(isRotating() ? "true" : "false");
                //printf("sample : %f \r \n",sample);
            }
            //printf("\r\n");
            if (test > 28.00f && oldFanSpeed != fanSpeed[4].Speed) {
                FanPWM.write(fanSpeed[4].Speed);
                oldFanSpeed = fanSpeed[4].Speed;
                //FanPWM.write(fanSpeed[3].Speed);
                //tft.setCursor(40, 70);
                tft.drawRect(45,42,75,16,ST7735_BLACK);
                tft.fillRect(45,42,75,16,ST7735_BLACK);
                tft.setTextColor(ST7735_RED);
                //tft.drawRect(40,70,125,20,ST7735_BLACK);
                //tft.fillRect(40,70,125,20,ST7735_BLACK);
                tft.printf(fanSpeed[4].Name);
                //sample = input.read();
                //printf("sample : %f \r \n",sample);
                //printf(isRotating() ? "true" : "false");
            }
        }
        //tft.drawRectangle(20,85,40,105,GREEN);
        //debugUSB.printf("Check Buffer \n\r");
        if (bufferFull) {
            //pc.printf("%d\n",bufferFull);
            //device.putc(rx_buffer[0]);
            debugUSB.printf("Buffer : %s\n\r", rx_buffer);
            //device.putc(*rx_buffer);
            bufferFull = false;
            rx = 0;
            if (strcmp("**PC_8=1",rx_buffer) == 0) {
                myled=1;
                tft.drawRect(27,100,30,10,ST7735_BLACK);
                tft.fillRect(27,100,30,10,ST7735_BLACK);
                tft.setTextColor(ST7735_GREEN);
                tft.setCursor(30, 100);
                tft.setTextSize(1);
                tft.printf("ON");

            } else
            if (strcmp("**PC_8=0",rx_buffer) == 0) {
                myled = 0;
                tft.drawRect(27,100,30,10,ST7735_BLACK);
                tft.fillRect(27,100,30,10,ST7735_BLACK);
                tft.setTextColor(ST7735_WHITE);
                tft.setCursor(30, 100);
                tft.setTextSize(1);
                tft.printf("OFF");

            } else
            if (strcmp("*PC_11=1",rx_buffer) == 0) {
                myled2=1;
                tft.drawRect(77,100,30,10,ST7735_BLACK);
                tft.fillRect(77,100,30,10,ST7735_BLACK);
                tft.setTextColor(ST7735_GREEN);
                tft.setCursor(80, 100);
                tft.setTextSize(1);
                tft.printf("ON");
                
            } else
            if (strcmp("*PC_11=0",rx_buffer) == 0) {
                myled2 = 0;
                tft.drawRect(77,100,30,10,ST7735_BLACK);
                tft.fillRect(77,100,30,10,ST7735_BLACK);
                tft.setTextColor(ST7735_WHITE);
                tft.setCursor(80, 100);
                tft.setTextSize(1);
                tft.printf("OFF");
                
            } else
            if (strcmp("*PC_12=1",rx_buffer) == 0) {
                myled3=1;
                tft.drawRect(27,135,30,10,ST7735_BLACK);
                tft.fillRect(27,135,30,10,ST7735_BLACK);
                tft.setTextColor(ST7735_GREEN);
                tft.setCursor(30, 135);
                tft.setTextSize(1);
                tft.printf("ON");
            } else
            if (strcmp("*PC_12=0",rx_buffer) == 0) {
                myled3 = 0;
                tft.drawRect(27,135,30,10,ST7735_BLACK);
                tft.fillRect(27,135,30,10,ST7735_BLACK);
                tft.setTextColor(ST7735_WHITE);
                tft.setCursor(30, 135);
                tft.setTextSize(1);
                tft.printf("OFF");
            } else
            if (strcmp("**PD_2=1",rx_buffer) == 0) {
                myled4 = 1;
                tft.drawRect(77,135,30,10,ST7735_BLACK);
                tft.fillRect(77,135,30,10,ST7735_BLACK);
                tft.setTextColor(ST7735_GREEN);
                tft.setCursor(80, 135);
                tft.setTextSize(1);
                tft.printf("ON");
            } else
            if (strcmp("**PD_2=0",rx_buffer) == 0) {
                myled4 = 0;
                tft.drawRect(77,135,30,10,ST7735_BLACK);
                tft.fillRect(77,135,30,10,ST7735_BLACK);
                tft.setTextColor(ST7735_WHITE);
                tft.setCursor(80, 135);
                tft.setTextSize(1);
                tft.printf("OFF");
            } else
            // not implemewnted yet
             debugUSB.printf("Buffer : %s <-- Not Implemented Yet!", rx_buffer);
        }

//        if(pc.readable()) {
////            device.putc(pc.getc());
//            pc.putc(pc.getc());
//        }
//        if(device.readable()) {
//            pc.putc(device.getc());
//        }
    }
}

// Interupt Routine to read in data from serial port

